# Supporting Files for 08348 (Languages and their Compilers) Laboratory and Assessed Coursework

Muyiwa Adesanmi Olu-Ogunleye, Department of Computer Science

# Introduction

The included files are for submission for the 08348 assessed coursework half of Languages and their Compilers module.

This document is written in [Markdown format](https://daringfireball.net/projects/markdown/) but should be perfectly readable in plain text. If you would like to view a formatted version of this document - which is not required - you may put it through a [Markdown formatter](http://dillinger.io/).

# Inventory

```
├── spl.bnf     - The BNF representation of SPL
├── spl.c       - A .c file included in the lexer and parser to output errors and set the debug mode
├── spl.l       - The lexical analysis file, meant to be run through flex
└── spl.y       - The parser file, meant to be run through bison
```

## The Lexer 

The lexer file includes all the valid tokens of the simple programming language. Any non-simple tokens, such as a range of values, are represented by regular expressions. The format of the document is targetted at being using the lexical analyser [flex](https://github.com/westes/flex).

To run the lexer, enter the command: `flex spl.l` which will generate a `lex.yy.c` file. 

This file, with no extra parameters is ready to be with the parser.

## Options

The lexer has a single option 'PRINT' which can be invoked during the `gcc build.

If you would like to build the lexical analyser on its own and print the tokens, run the following command: `gcc -o spl lex.yy.c -DPRINT`.

## The Parser

The parser file contains all the rules to transform the tokens given to the lexer to valid C output. The parser file is meant to be used with the [bison](https://www.gnu.org/software/bison/) parser.

To build the parser file, run the following command `bison spl.y` which generates a `spl.tab.c` file representing the rules of the program in C format. 

To generate a compiler binary from the parser file, run `gcc -o spl spl.tab.c spl.c -ll` (the flag `lfl` should be used for windows). This builds the parser file (which includes the lexer file in the source code) along with common headers to output errors for the compiler.

## Options

The parser has a single option 'DEBUG' which can be invoked during the `gcc` build. An example is: `gcc -o spl spl.tab.c spl.c -ll -DDEBUG`.

__Please Note:__ The output from the DEBUG option will have the debug data used for a visual representation of the SPL program's tree and is unsuitable for generating a binary representing the SPL program in C.

## The Compiler

The resulting file from the above commands is an SPL to C compiler. This compiler accepts input in the form of valid SPL programs (note that the extension doesn't matter) and outputs a C representation of that input. 

You can pipe the output to an external file and build the program with the following command (assuming that the input is located in `../examples/a.SPL` and the output will be at `../output/a.c`) `./spl < ../examples/a.SPL > ../output/a.c && gcc -o ../output/a ../output/a.c`. 

Running `.././a` will execute the SPL program.

# Error Handling

## Optimisation
The parser does a two stage run through of the program. The first stage analyses the program to see if any syntax errors have occured. The second stage then runs through the entire program and prints out a C representation of the input. The program will not print if any errors have occured in the code.
