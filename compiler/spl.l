%{
#ifdef PRINT
#define TOKEN(t) printf("Token: " #t "\n");
#define TEXT(t) printf("Token: " #t " %s\n", yytext);
#define NUMBER_CONSTANT(t) printf("Token: " #t " %d\n", atoi(yytext));
#define REAL_CONSTANT(t) printf("Token: " #t " %f\n", atof(yytext));
#else
#define TOKEN(t) return(t);
#define TEXT(t) yylval.iVal = installId(yytext); return(t);
#define NUMBER_CONSTANT(t) yylval.iVal = atoi(yytext); return(t);
#define REAL_CONSTANT(t) yylval.iVal = installId(yytext); return(t);

#include <string.h>
extern SYMTABNODEPTR symTab[SYMTABSIZE];
extern int currentSymTabSize;
int installId(char *id);
int lookup(char *s);

#endif
%}

delim            [ \t\r\n]
ws               {delim}+
digit            [0-9]
digits           {digit}+
character        [A-Za-z]
identifier       ([A-Za-z])([A-Za-z0-9]*)
number_constant  {digits}
real_constant    {digits}"."{digits}
character_constant '{character}'
%%
{ws}            ;

                /* Operators */
"+"             TOKEN(PLUS);
"*"             TOKEN(MULTIPLY);
"-"             TOKEN(SUBTRACT);
"/"             TOKEN(DIVIDE);
"->"            TOKEN(ACCESSOR);
"="             TOKEN(EQUALS);
"<>"            TOKEN(NOT_EQUALS);
"<"             TOKEN(LESS_THAN);
">"             TOKEN(GREATER_THAN);
"<="            TOKEN(LESS_THAN_EQUALS_TO);
">="            TOKEN(GREATER_THAN_EQUALS_TO);

                /* Characters */
"("             TOKEN(LEFT_BRA);
")"             TOKEN(RIGHT_KET);
";"             TOKEN(SEMI_COLON);
"{"             TOKEN(LEFT_BRACE);
"}"             TOKEN(RIGHT_BRACE);
":"             TOKEN(COLON);
","             TOKEN(COMMA);
"'"             TOKEN(APROSTROPHE);
"."             TOKEN(DOT);

                /* Keywords */
"OF"              TOKEN(OF);
"TYPE"            TOKEN(TYPE);
"CHARACTER"       TOKEN(CHARACTER);
"INTEGER"         TOKEN(INTEGER);
"REAL"            TOKEN(REAL);
"IF"              TOKEN(IF);
"ELSE"            TOKEN(ELSE);
"THEN"            TOKEN(THEN);
"ENDIF"           TOKEN(ENDIF);
"DO"              TOKEN(DO);
"WHILE"           TOKEN(WHILE);
"ENDDO"           TOKEN(ENDDO);
"ENDWHILE"        TOKEN(ENDWHILE);
"FOR"             TOKEN(FOR);
"IS"              TOKEN(IS);
"BY"              TOKEN(BY);
"TO"              TOKEN(TO);
"ENDFOR"          TOKEN(ENDFOR);
"WRITE"           TOKEN(WRITE);
"NEWLINE"         TOKEN(NEWLINE);
"READ"            TOKEN(READ);
"NOT"             TOKEN(NOT);
"AND"             TOKEN(AND);
"OR"              TOKEN(OR);
"CODE"            TOKEN(CODE);
"ENDP"            TOKEN(ENDP);
"DECLARATIONS"    TOKEN(DECLARATIONS);


{identifier}      {
                   TEXT(IDENTIFIER);
                  };
{real_constant}   {
                   REAL_CONSTANT(REAL_CONSTANT);                 
                  };
{number_constant} {
                    NUMBER_CONSTANT(NUMBER_CONSTANT);
                  };
{character_constant} {
                       TEXT(CHARACTER_CONSTANT);
                     };
.                   printf("Error: unexpected symbol in lexical analyser %s (%d)\n", yytext, yytext[0]);
%%
#ifndef PRINT
SYMTABNODEPTR newSymTabNode()
{
    return ((SYMTABNODEPTR)malloc(sizeof(SYMTABNODE)));
}


int installId(char *id) 
{
    extern SYMTABNODEPTR symTab[SYMTABSIZE]; 
    extern int currentSymTabSize;
    int index;

    index = lookup(id);
    if (index >= 0)
    {
        return (index);
    }
    else 
       if (currentSymTabSize >= SYMTABSIZE) 
          return (NOTHING) ;
    else
    {
       symTab[currentSymTabSize] = newSymTabNode();
       strncpy(symTab[currentSymTabSize]->identifier,id,IDLENGTH);
       symTab[currentSymTabSize]->identifier[IDLENGTH-1] = '\0';
       return(currentSymTabSize++);
    }
}

int lookup(char *s)
{
    extern SYMTABNODEPTR symTab[SYMTABSIZE];
    extern int currentSymTabSize;
    int i;

    for(i=0; i<currentSymTabSize; i++)
    {
        if(strncmp(s,symTab[i]->identifier,IDLENGTH) == 0)
        {
            return (i);
        }
    }
    return (-1);    
}
#endif
