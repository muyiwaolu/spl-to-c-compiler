%{
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int yylex ();
void yyerror();

#define SYMTABSIZE  50
#define IDLENGTH    15
#define TYPELENGTH  25
#define NOTHING     -1
#define IDENTOFFSET 2
#define TOTALIDLENGTH 50
enum ParseTreeNodeType { PROGRAM, BLOCK, DECLARATION_BLOCK, COMMA_IDENTIFIERS, IDENTIFIERS, CHARACTER_TYPE, INTEGER_TYPE, REAL_TYPE,  STATEMENT_LIST, STATEMENT, ASSIGNMENT_STATEMENT, IF_STATEMENT, ELSE_STATEMENT, DO_STATEMENT, WHILE_STATEMENT, FOR_STATEMENT, FOR_IS_BY_TO_STATEMENT, FOR_STATEMENT_LIST, WRITE_STATEMENT, NEWLINE_STATEMENT, READ_STATEMENT, OUTPUT_LIST, AND_CONDITIONAL, OR_CONDITIONAL, COMP_CONDITIONAL, EXPRESSION_COMPARATOR, NOT_EXPRESSION_COMPARATOR, COMPARATOR, EXPRESSION, TERM, VALUE, CONSTANT, NUMBER, ID_REAL, CHARACTER_VALUE, IDENTIFIER_VALUE, MULTIPLY_TERM, DIVIDE_TERM, VALUE_TERM, ADDITION_EXPRESSION, SUBTRACTION_EXPRESSION, TERM_EXPRESSION, EQUALS_COMPARATOR, GREATER_THAN_EQUALS_TO_COMPARATOR, LESS_THAN_EQUALS_TO_COMPARATOR, LESS_THAN_COMPARATOR, GREATER_THAN_COMPARATOR, NOT_COMPARATOR, NOT_EQUALS_COMPARATOR, NUMBER_CONSTANT_VALUE, REAL_CONSTANT_VALUE, CONSTANT_VALUE, EXPRESSION_VALUE, SUBTRACT_NUMBER, SUBTRACT_ID_REAL };

char *NodeNames[] = { "PROGRAM", "BLOCK",  "DECLARATION_BLOCK", "COMMA_IDENTIFIERS", "IDENTIFIERS", "CHARACTER_TYPE", "INTEGER_TYPE", "REAL_TYPE",  "STATEMENT_LIST", "STATEMENT", "ASSIGNMENT_STATEMENT", "IF_STATEMENT", "ELSE_STATEMENT", "DO_STATEMENT", "WHILE_STATEMENT", "FOR_STATEMENT", "FOR_IS_BY_TO_STATEMENT", "FOR_STATEMENT_LIST", "WRITE_STATEMENT", "NEWLINE_STATEMENT", "READ_STATEMENT", "OUTPUT_LIST", "AND_CONDITIONAL", "OR_CONDITIONAL", "COMP_CONDITIONAL", "EXPRESSION_COMPARATOR", "NOT_EXPRESSION_COMPARATOR", "COMPARATOR", "EXPRESSION", "TERM", "VALUE", "CONSTANT", "NUMBER", "ID_REAL", "CHARACTER_VALUE", "IDENTIFIER_VALUE", "MULTIPLY_TERM", "DIVIDE_TERM", "VALUE_TERM", "ADDITION_EXPRESSION", "SUBTRACTION_EXPRESSION", "TERM_EXPRESSION", "EQUALS_COMPARATOR", "GREATER_THAN_EQUALS_TO_COMPARATOR", "LESS_THAN_EQUALS_TO_COMPARATOR", "LESS_THAN_COMPARATOR", "GREATER_THAN_COMPARATOR", "NOT_COMPARATOR", "NOT_EQUALS_COMPARATOR", "NUMBER_CONSTANT_VALUE", "REAL_CONSTANT_VALUE", "CONSTANT_VALUE", "EXPRESSION_VALUE", "SUBTRACT_NUMBER", "SUBTRACT_ID_REAL" };


#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL 0
#endif

struct treeNode {
    int item;
    int nodeIdentifier;
    struct treeNode *first;
    struct treeNode *second;
    struct treeNode *third;
};

typedef struct treeNode TREE_NODE;
typedef TREE_NODE       *TERNARY_TREE;

TERNARY_TREE create_node(int, int, TERNARY_TREE, TERNARY_TREE, TERNARY_TREE);

#ifdef DEBUG
void PrintTree(TERNARY_TREE t, int indent);
#else
void GenerateCode(TERNARY_TREE t);
void AnalyseProgram(TERNARY_TREE t);
#endif

int yylex(void);
void yyerror(char *s);

struct symTabNode {
    char identifier[IDLENGTH];
    int type;
    int intVal;
};

typedef struct symTabNode SYMTABNODE;
typedef SYMTABNODE        *SYMTABNODEPTR;

SYMTABNODEPTR symTab[SYMTABSIZE];

int currentSymTabSize = 0;
bool error = false;
%}
%start  program

%union {
    int iVal;
    TERNARY_TREE  tVal;
}
%token<iVal> IDENTIFIER REAL_CONSTANT NUMBER_CONSTANT CHARACTER_CONSTANT

%type<tVal> program block declaration_block identifiers type statement_list statement assignment_statement if_statement else_statement do_statement while_statement for_statement write_statement read_statement output_list conditional expression_comparator comparator expression term value constant number real number_constant

%token PLUS MULTIPLY SUBTRACT DIVIDE ACCESSOR EQUALS NOT_EQUALS LESS_THAN GREATER_THAN LESS_THAN_EQUALS_TO GREATER_THAN_EQUALS_TO LEFT_BRA RIGHT_KET SEMI_COLON LEFT_BRACE RIGHT_BRACE COLON COMMA APROSTROPHE DOT OF TYPE CHARACTER INTEGER REAL IF ELSE THEN ENDIF DO WHILE ENDDO ENDWHILE FOR IS BY TO ENDFOR WRITE NEWLINE READ NOT AND OR CODE ENDP DECLARATIONS

%%

program: IDENTIFIER COLON block ENDP IDENTIFIER DOT
       {
           TERNARY_TREE ParseTree;
           ParseTree = create_node($1, PROGRAM, $3, create_node($5, PROGRAM, NULL, NULL, NULL), NULL);
#ifdef DEBUG
           PrintTree(ParseTree, 0);
#else
           AnalyseProgram(ParseTree);
           if(! error)
               GenerateCode(ParseTree);
#endif
}
       ;

block:  DECLARATIONS declaration_block CODE statement_list
     {
       $$ = create_node(NOTHING, BLOCK, $2, $4, NULL);
     }
     | CODE statement_list
     {
       $$ = create_node(NOTHING, BLOCK, $2, NULL, NULL);
     }
     ;

declaration_block: identifiers OF TYPE type SEMI_COLON
                 {
                     $$ = create_node(NOTHING, DECLARATION_BLOCK, $1, $4, NULL);
                 }
                 | identifiers OF TYPE type SEMI_COLON declaration_block
                 {
                     $$ = create_node(NOTHING, DECLARATION_BLOCK, $1, $4, $6);
                 }
                 ;

identifiers: IDENTIFIER COMMA identifiers
           {
               $$ = create_node($1, COMMA_IDENTIFIERS, $3, NULL, NULL);
           }
           | IDENTIFIER
           {
               $$ = create_node($1, IDENTIFIERS, NULL, NULL, NULL);
           }
           ;

type: CHARACTER
    {
          $$ = create_node(CHARACTER, CHARACTER_TYPE, NULL, NULL, NULL);
      }
      | INTEGER
      {
          $$ = create_node(INTEGER, INTEGER_TYPE, NULL, NULL, NULL);
      }
      | REAL
      {
          $$ = create_node(REAL, REAL_TYPE, NULL, NULL, NULL);
      }
      ;

statement_list: statement SEMI_COLON statement_list
              {
                    $$ = create_node(NOTHING, STATEMENT_LIST, $1, $3, NULL);
                }
                | statement
                {
                    $$ = create_node(NOTHING, STATEMENT_LIST, $1, NULL, NULL);
                }
                ;

statement: assignment_statement
         {
            $$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
         }
         | if_statement
         {
            $$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
         }
         | do_statement
         {
            $$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
         }
         | while_statement
         {
            $$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
         }
         | for_statement
         {
            $$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
         }
         | write_statement
         {
            $$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
         }
         | read_statement
         {
            $$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
         }
         ;

assignment_statement: expression ACCESSOR IDENTIFIER
                    {
                         $$ = create_node($3, ASSIGNMENT_STATEMENT, $1, NULL, NULL);
                      }
                      ;

if_statement: IF conditional THEN statement_list else_statement ENDIF
            {
                 $$ = create_node(NOTHING, IF_STATEMENT, $2, $4, $5);
              }
              | IF conditional THEN statement_list ENDIF
              {
                 $$ = create_node(NOTHING, IF_STATEMENT, $2, $4, NULL);
              }
              ;

else_statement: ELSE statement_list
              {
                 $$ = create_node(NOTHING, ELSE_STATEMENT, $2, NULL, NULL);
              }
              ;

do_statement: DO statement_list WHILE conditional ENDDO
            {
                $$ = create_node(NOTHING, DO_STATEMENT, $2, $4, NULL);
             }
             ;

while_statement: WHILE conditional DO statement_list ENDWHILE
               {
                     $$ = create_node(NOTHING, WHILE_STATEMENT, $2, $4, NULL);
                 }
                 ;

for_statement: FOR IDENTIFIER IS expression BY expression TO expression DO statement_list ENDFOR
             {
                   $$ = create_node($2, FOR_IS_BY_TO_STATEMENT, $4, $6, create_node(NOTHING, FOR_IS_BY_TO_STATEMENT, $8, $10, NULL));
              }
              ;

write_statement: WRITE LEFT_BRA output_list RIGHT_KET
               {
                    $$ = create_node(NOTHING, WRITE_STATEMENT, $3, NULL, NULL);
                 }

| NEWLINE
                 {
                    $$ = create_node(NOTHING, NEWLINE_STATEMENT, NULL, NULL, NULL);
                 }
               ;

read_statement: READ LEFT_BRA IDENTIFIER RIGHT_KET
              {
                   $$ = create_node($3, READ_STATEMENT, NULL, NULL, NULL);
                }
                ;

output_list: value
           {
                $$ = create_node(NOTHING, OUTPUT_LIST, $1, NULL, NULL);
             }
             | value COMMA output_list
             {
                $$ = create_node(NOTHING, OUTPUT_LIST, $1, $3, NULL);
             }
             ;
conditional: expression_comparator AND conditional
           {
                $$ = create_node(NOTHING, AND_CONDITIONAL, $1, $3, NULL);
             }
             | expression_comparator OR conditional
             {
                $$ = create_node(NOTHING, OR_CONDITIONAL, $1, $3, NULL);
             }
             | expression_comparator
             {
                $$ = create_node(NOTHING, COMP_CONDITIONAL, $1, NULL, NULL);
             }
             ;

expression_comparator: expression comparator expression
                     {
                        $$ = create_node(NOTHING, EXPRESSION_COMPARATOR, $1, $2, $3);
                     }
                     | NOT  expression_comparator
                     {
                        $$ = create_node(NOTHING, NOT_EXPRESSION_COMPARATOR, $2, NULL, NULL);
                     }
                    ;

comparator: EQUALS
          {
              $$ = create_node(NOTHING, EQUALS_COMPARATOR, NULL, NULL, NULL);
          }
          | GREATER_THAN_EQUALS_TO
          {
              $$ = create_node(NOTHING, GREATER_THAN_EQUALS_TO_COMPARATOR, NULL, NULL, NULL);
          }
          | LESS_THAN_EQUALS_TO
          {
              $$ = create_node(NOTHING, LESS_THAN_EQUALS_TO_COMPARATOR, NULL, NULL, NULL);
          }
          | LESS_THAN
          {
              $$ = create_node(NOTHING, LESS_THAN_COMPARATOR, NULL, NULL, NULL);
          }
          | GREATER_THAN
          {
              $$ = create_node(NOTHING, GREATER_THAN_COMPARATOR, NULL, NULL, NULL);
          }
          | NOT
          {
              $$ = create_node(NOTHING, NOT_COMPARATOR, NULL, NULL, NULL);
          }
          | NOT_EQUALS
          {
              $$ = create_node(NOTHING, NOT_EQUALS_COMPARATOR, NULL, NULL, NULL);
          }
          ;

expression: term PLUS expression
          {
              $$ = create_node(NOTHING, ADDITION_EXPRESSION, $1, $3, NULL);
          }
          | term SUBTRACT expression
          {
              $$ = create_node(NOTHING, SUBTRACTION_EXPRESSION, $1, $3, NULL);
          }
          | term
          {
              $$ = create_node(NOTHING, TERM_EXPRESSION, $1, NULL, NULL);
          }
          ;

term: value MULTIPLY term
    {
          $$ = create_node(NOTHING, MULTIPLY_TERM, $1, $3, NULL);
      }
      | value DIVIDE term
      {
          $$ = create_node(NOTHING, DIVIDE_TERM, $1, $3, NULL);
      }
      | value
      {
          $$ = create_node(NOTHING, VALUE_TERM, $1, NULL, NULL);
      }
      ;

value: IDENTIFIER
     {
           $$ = create_node($1, IDENTIFIER_VALUE, NULL, NULL, NULL);
       }
       | constant
       {
           $$ = create_node(NOTHING, CONSTANT_VALUE, $1, NULL, NULL);
       }
       | LEFT_BRA expression RIGHT_KET
       {
           $$ = create_node(NOTHING, EXPRESSION_VALUE, $2, NULL, NULL);
       }
       ;

constant: number_constant {
        $$ = create_node(NOTHING, CONSTANT, $1, NULL, NULL);
          }
          | CHARACTER_CONSTANT
          {
              $$ = create_node($1, CHARACTER_VALUE, NULL, NULL, NULL);
          }
          ;

number_constant: number
               {
  $$ = create_node(NOTHING, NUMBER_CONSTANT_VALUE, $1, NULL, NULL);
}
| real
{
  $$ = create_node(NOTHING, REAL_CONSTANT_VALUE, $1, NULL, NULL);
};

number: SUBTRACT NUMBER_CONSTANT
      {
            $$ = create_node($2, SUBTRACT_NUMBER, NULL, NULL, NULL);
        }
        | NUMBER_CONSTANT
        {
            $$ = create_node($1, NUMBER, NULL, NULL, NULL);
        }
        ;

real: SUBTRACT REAL_CONSTANT
    {
          $$ = create_node($2, SUBTRACT_ID_REAL, NULL, NULL, NULL);
      }
      | REAL_CONSTANT
      {
          $$ = create_node($1, ID_REAL, NULL, NULL, NULL);
      }
      ;

%%
/* Creates a TERNARY_TREE node */
TERNARY_TREE create_node(int iVal, int case_identifier, TERNARY_TREE p1, TERNARY_TREE p2, TERNARY_TREE p3)
{
    TERNARY_TREE t;
    t = (TERNARY_TREE)malloc(sizeof(TREE_NODE));
    t->item = iVal;
    t->nodeIdentifier = case_identifier;
    t->first = p1;
    t->second = p2;
    t->third = p3;
    return (t);
}



#ifdef DEBUG
/* Prints out a tree representing the TERNARY_TREE t in a (apparently) pretty format */
void PrintTree(TERNARY_TREE t, int indent)
{
   if (t == NULL) return;

/* Indentation */
   int i = 0;
   for(;i < indent; i++) {
       printf(" ");
   }

/* Print out the Node Identifier */
   if(t->nodeIdentifier < 0 || t->nodeIdentifier > sizeof(NodeNames)) {
       printf("Unknown nodeIdentifier: %d\n", t->nodeIdentifier);
   } else {
       printf("nodeIdentifier: %s\n", NodeNames[t->nodeIdentifier]);
   }

/* Indentation */
   if(t->nodeIdentifier == NUMBER           |
      t->nodeIdentifier == SUBTRACT_NUMBER  |
      t->nodeIdentifier == ID_REAL          |
      t->nodeIdentifier == SUBTRACT_ID_REAL |
      t->nodeIdentifier == IDENTIFIER_VALUE |
      t->nodeIdentifier == CHARACTER_VALUE)
 {
       int i = 0;
       for(; i < indent; i++) {
           printf(" ");
       }
   }

/* Print out any extra values (items off the symbol table, etc.) */
   if(t->nodeIdentifier == NUMBER) {
       printf("Number: %i\n", t->item);
   }

   if(t->nodeIdentifier == SUBTRACT_NUMBER) {
       printf("Negative Number: -%i\n", t->item);
   }

   if(t->nodeIdentifier == ID_REAL) {
       printf("Real: %s\n", symTab[t->item]->identifier);
   }

   if(t->nodeIdentifier == SUBTRACT_ID_REAL) {
       printf("Negative Real: -%s\n", symTab[t->item]->identifier);
   }

   if(t->nodeIdentifier == IDENTIFIER_VALUE) {
       if(t->item > 0 && t->item < SYMTABSIZE) {
          printf("Identifier: %s\n", symTab[t->item]->identifier);
       } else {
          printf("Unknown identifier: %d\n", t->item);
       }
   }

  if(t->nodeIdentifier == CHARACTER_VALUE) {
       printf("Character: %s\n", symTab[t->item]->identifier);
   }

   PrintTree(t->first, indent + 2);
   PrintTree(t->second, indent + 2);
   PrintTree(t->third, indent + 2);
}
#else
/* Holds state about the printing state of the program */
/* Used for contextually printf-ing an identifier. */
bool isPrinting = false;

/* Holds state about if */
bool insideIf = false;

/* Holds state about expression style printing */
bool expressionPrint = false;
bool expressionAnalysing = false;

int typeOfExpression = -1;

/* Holds the state about the current idenfitier:

-1: nothing
 0: char
 1: int
 2: real
*/
int currentIdentifier = -1;

/* The identifier the represents the program */
int programID = -1;


/* Convenience function to check string equality */
bool StringEqual(const char* first, const char* second) {
   return strcmp(first, second) == 0;
}

/* Convenience function to invoke yyerror and print the relevant message */
void PrintError(char* message) {
   error = true;
   yyerror(message);
}

/* Prepends an underscore to an identifier to remove keyword collision. */
void PrintIdentifier(int itemID) {
   printf("_%s", symTab[itemID]->identifier);
} 


/* Code Analysis to make sure there are no discernable errors. */
void AnalyseProgram(TERNARY_TREE t)
{
   if (t == NULL) return;
   switch(t->nodeIdentifier) {
      case(PROGRAM):
         if(! StringEqual(symTab[t->item]->identifier, symTab[t->second->item]->identifier)){
             PrintError("Program Idenfitiers do not match.");
         }
         programID = t->item;
         AnalyseProgram(t->first);
         return;
      case(FOR_IS_BY_TO_STATEMENT):
         AnalyseProgram(t->first);
         AnalyseProgram(t->second);
         AnalyseProgram(t->third->first);
         AnalyseProgram(t->third->second);
         return;
      case(COMMA_IDENTIFIERS):
      case(IDENTIFIERS):
        if(StringEqual(symTab[programID]->identifier, symTab[t->item]->identifier)){
           PrintError("Identifier cannot match program identifier.");
        }
        break;
   }
   AnalyseProgram(t->first);
   AnalyseProgram(t->second);
   AnalyseProgram(t->third);
}

/* Generates C equivalent of a TERNARY_TREE representation of a SPL file */
void GenerateCode(TERNARY_TREE t)
{
   if (t == NULL) return;
   switch(t->nodeIdentifier) {
      case(PROGRAM):
         printf("#include <stdio.h>\nint main(){");
         GenerateCode(t->first);
         printf("return 0;");
         printf("}");
         return;
      case(BLOCK):
         GenerateCode(t->first);
         GenerateCode(t->second);
         return;
      case(DECLARATION_BLOCK):
         GenerateCode(t->second);
         GenerateCode(t->first);
         printf(";");
         GenerateCode(t->third);
         return;
      case(COMMA_IDENTIFIERS):
         PrintIdentifier(t->item);
         symTab[t->item]->type = currentIdentifier;
         printf(",");
         GenerateCode(t->first);
         return;
      case(IDENTIFIERS):
         PrintIdentifier(t->item);
         symTab[t->item]->type = currentIdentifier;
         return;
      case(CHARACTER_TYPE):
         printf("char ");
         currentIdentifier = 0;
         return;
      case(INTEGER_TYPE):
         printf("int ");
         currentIdentifier = 1;
         return;
      case(REAL_TYPE):
         printf("float ");
         currentIdentifier = 2;
         return;
      case(STATEMENT_LIST):
         GenerateCode(t->first);
         if(t->second != NULL) {
            GenerateCode(t->second);
         }
         return;
      case(STATEMENT):
         GenerateCode(t->first);
         return;
      case(ASSIGNMENT_STATEMENT):
         PrintIdentifier(t->item);
         printf(" = ");
         GenerateCode(t->first);
         if(! insideIf) {
             printf(";");
         }
         return;
      case(IF_STATEMENT):
         insideIf = true;
         printf("if (");
         GenerateCode(t->first);
         insideIf = false;
         printf(") {");
         GenerateCode(t->second);
         printf("}");
         GenerateCode(t->third);
         return;
      case(ELSE_STATEMENT):
         printf("else { ");
         GenerateCode(t->first);
         printf("}");
         return;
      case(DO_STATEMENT):
         printf("do{");
         GenerateCode(t->first);
         printf("} while (");
         GenerateCode(t->second);
         printf(");");
         return;
      case(WHILE_STATEMENT):
         printf("while(");
         GenerateCode(t->first);
         printf("){");
         GenerateCode(t->second);
         printf("}");
         return;
      case(FOR_STATEMENT):
         GenerateCode(t->first);
         GenerateCode(t->second);
         return;
      case(FOR_IS_BY_TO_STATEMENT):
         printf("if(");
         GenerateCode(t->second);
         printf(" > 0) {");
         printf("for(");
         PrintIdentifier(t->item);
         printf(" = ");
         GenerateCode(t->first);
         printf(";");
         PrintIdentifier(t->item);
         printf(" <= ");
         GenerateCode(t->third->first);
         printf(";");
         PrintIdentifier(t->item);
         printf(" += ");
         GenerateCode(t->second);
         printf("){");
         GenerateCode(t->third->second);
         printf("}");

         printf("} else {");
         printf("for("); 
         PrintIdentifier(t->item);
         printf(" = ");
         GenerateCode(t->first);
         printf(";");
         PrintIdentifier(t->item);
         printf(" >= ");
         GenerateCode(t->third->first);
         printf(";");
         PrintIdentifier(t->item);
         printf(" += ");
         GenerateCode(t->second);
         printf("){");
         GenerateCode(t->third->second);
         printf("}");
         printf("}");
         return;
      case(WRITE_STATEMENT):
        isPrinting = true;
        GenerateCode(t->first);
        isPrinting = false;
        return;
      case(NEWLINE_STATEMENT):
        printf("printf(\"\\n\");");
        return;
      case(READ_STATEMENT):
      {
        if(symTab[t->item]->type == 0) {
           printf("scanf(\"%%c\", ");
        }
        if(symTab[t->item]->type == 1) {
           printf("scanf(\"%%i\", ");
        }
        if(symTab[t->item]->type == 2) {
           printf("scanf(\"%%f\", ");
        }
        printf("&");
        PrintIdentifier(t->item);
        printf(");");
        return;
      }
      case(OUTPUT_LIST):
        GenerateCode(t->first);
        GenerateCode(t->second);
        return;
      case(AND_CONDITIONAL):
        GenerateCode(t->first);
        printf(" && ");
        GenerateCode(t->second);
        return;
      case(OR_CONDITIONAL):
        GenerateCode(t->first);
        printf(" || ");
        GenerateCode(t->second);
        return;
      case(COMP_CONDITIONAL):
        GenerateCode(t->first);
        GenerateCode(t->second);
        return;
      case(EXPRESSION_COMPARATOR):
        GenerateCode(t->first);
        GenerateCode(t->second);
        GenerateCode(t->third);
        return;
      case(NOT_EXPRESSION_COMPARATOR):
        printf("!(");
        GenerateCode(t->first);
        printf(")");
        return;
      case(EQUALS_COMPARATOR):
        printf("==");
        return;
      case(GREATER_THAN_EQUALS_TO_COMPARATOR):
        printf(">=");
        return;
      case(LESS_THAN_EQUALS_TO_COMPARATOR):
        printf("<=");
        return;
      case(LESS_THAN_COMPARATOR):
        printf("<");
        return;
      case(GREATER_THAN_COMPARATOR):
        printf(">");
        return;
      case(NOT_COMPARATOR):
        /* TODO: This is redundant */
        printf("!");
        return;
      case(NOT_EQUALS_COMPARATOR):
        printf("!=");
        return;
      case(ADDITION_EXPRESSION):
        GenerateCode(t->first);
        if(t->second != NULL) {
            if(! expressionAnalysing)
                printf("+");
            GenerateCode(t->second);
        }
        return;
      case(SUBTRACTION_EXPRESSION):
        GenerateCode(t->first);
        if(t->second != NULL) {
            if(! expressionAnalysing)
                printf("-");
            GenerateCode(t->second);
        }
        return;
      case(TERM_EXPRESSION):
        GenerateCode(t->first);
        return;
      case(MULTIPLY_TERM):
        GenerateCode(t->first);
        if(t->second != NULL) {
            if(! expressionAnalysing)
                printf("*");
            GenerateCode(t->second);
        }
        return;
      case(DIVIDE_TERM):
        GenerateCode(t->first);
        if(t->second != NULL) {
            if(! expressionAnalysing)
                printf("/");
            GenerateCode(t->second);
        }
        return;
      case(VALUE_TERM):
        GenerateCode(t->first);
        return;
      case(IDENTIFIER_VALUE):
      {
        if(isPrinting && !expressionPrint && !expressionAnalysing) {
          if(symTab[t->item]->type == 0) {
               printf("printf(\"%%c\", ");
          }
          if(symTab[t->item]->type == 1) {
               printf("printf(\"%%i\", ");
          }
          if(symTab[t->item]->type == 2) {
               printf("printf(\"%%f\", ");
          }
        }
        
        if(expressionAnalysing && typeOfExpression == -1){
          if(symTab[t->item]->type == 0) {
              typeOfExpression = 0;
          }
          if(symTab[t->item]->type == 1) {
              typeOfExpression = 1;
          }
          if(symTab[t->item]->type == 2) {
              typeOfExpression = 2;
          }
        }
        
        if(!expressionAnalysing)
          PrintIdentifier(t->item);

/* Complete the printf statement, if we are coming from
           a print node */
        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf(");");
        }
        return;
      }
      case(CONSTANT_VALUE):
        GenerateCode(t->first);
        return;
      case(EXPRESSION_VALUE):
        if(isPrinting) {
          expressionAnalysing = true;
          GenerateCode(t->first);
          expressionAnalysing = false;
          if(typeOfExpression == 0) {
               printf("printf(\"%%c\", ");
          }
          if(typeOfExpression == 1) {
               printf("printf(\"%%i\", ");
          }
          if(typeOfExpression == 2) {
               printf("printf(\"%%f\", ");
          }
          typeOfExpression = -1;
        } else {
           printf("(");
        }

        expressionPrint = true;
        GenerateCode(t->first);
        expressionPrint = false;
          printf(")");
        if(isPrinting) {
          printf(";");
        }
        return;
      case(CONSTANT):
        GenerateCode(t->first);
        return;
      case(CHARACTER_VALUE):
        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf("printf(\"%%c\", ");
        }
        if(expressionAnalysing && typeOfExpression == -1){
           typeOfExpression = 0;
        }
        if(!expressionAnalysing)
          printf("%s", symTab[t->item]->identifier);

        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf(");");
        }
        return;
      case(NUMBER_CONSTANT_VALUE):
        GenerateCode(t->first);
        return;
      case(REAL_CONSTANT_VALUE):
        GenerateCode(t->first);
        return;
      case(SUBTRACT_NUMBER):
        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf("printf(\"%%i\", ");
        }

        if(expressionAnalysing && typeOfExpression == -1){
           typeOfExpression = 1;
        }
        if(!expressionAnalysing)
            printf("-%i", t->item);

        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf(");");
        }
        return;
      case(NUMBER):
        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf("printf(\"%%i\", ");
        }

        if(expressionAnalysing && typeOfExpression == -1){
           typeOfExpression = 1;
        }

        if(!expressionAnalysing)
            printf("%i", t->item);

        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf(");");
        }
        return;
      case(SUBTRACT_ID_REAL):
        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf("printf(\"%%f\", ");
        }

        if(expressionAnalysing && typeOfExpression == -1){
           typeOfExpression = 1;
        }

        if(!expressionAnalysing)
            printf("-%s", symTab[t->item]->identifier);

        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf(");");
        }
        return;
      case(ID_REAL):
        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf("printf(\"%%f\", ");
        }

        if(expressionAnalysing && typeOfExpression == -1){
           typeOfExpression = 1;
        }

        if(!expressionAnalysing)
            printf("%s", symTab[t->item]->identifier);

        if(isPrinting && !expressionPrint && !expressionAnalysing) {
           printf(");");
        }
        return;
   }
}
#endif
#include "lex.yy.c"
