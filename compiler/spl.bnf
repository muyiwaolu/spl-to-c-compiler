/* Backus Naur Representation of the Simple Programming Language (SPL) */

<digit>::= 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |9
<digits>::= <digit> | <digits> <digit>

<character>::= A | B | C | D | E | F | G | H | I | J | K | L | M | N | O | P | Q | R | S | T | U | V | W | X | Y | Z |
               a | b | c | d | e | f | g | h | i | j | k | l | m | n | o | p | q | r | s | t | u | v | w | x | y | z

<identifier>::= <character>
                | <identifier> <digit>
                | <identifier> <character>

<number_constant>::= "-" <digits>
                     | <digits> 
                     | <digits> "." <digits>
                     | "-" <digits> "." <digits>

<character_constant>::= "'" <character> "'"

<constant>::= <number_constant> | <character_constant>

<value>::= <identifier>
           | <constant>
           | "(" <expression> ")"

<values>::= <value> | <values> <value>

<term>::= <value> "*" <term>
          | <value> "/" <term>
          | <value>

<expression>::= <term> "+" <expression>
                | <term> - <expression>
                | <term>

<comparator>::= "=" | "<>" | "<" | ">" | "<=" | ">="

<expression_comparator>::= <expression> <comparator> <expression> | NOT <expression_comparator>
<conditional>::=  <expression_comparator> AND <conditional> |  <expression_comparator> OR <conditional> | <expression_comparator> 

<output_list>::= <value> | <value> "," <output_list>

<read_statement>::= READ "(" <identifier> ")"

<write_statement>::= WRITE "(" <identifier> ")" | NEWLINE

<for_statement>::= FOR <identifier> IS <expression> BY <expression> TO <expression> DO <statement_list> ENDFOR

<while_statement>::= WHILE <conditional> DO <statement_list> ENDWHILE

<do_statement>::= DO <statement_list> WHILE <conditional> ENDDO

<else_statement>::= ELSE <statement_list>

<if_statement>::= IF <conditional> THEN <statement_list> <else_statement> ENDIF | IF <conditional> THEN <statement_list> ENDIF

<assignment_statement>::= <expression> "->" <identifier>

<statement>::= <assignment_statement>
               | <if_statement>
               | <do_statement>
               | <while_statement>
               | <for_statement>
               | <write_statement>
               | <read_statement>
<statement_list>::= <statement> ";" <statement_list> | <statement>

<type>::= CHARACTER | INTEGER | REAL

<identifiers>::= <identifier> "," <identifiers> | <identifier>

<declaration_block>::= <identifiers> OF TYPE <type> ";" | <identifiers> OF TYPE <type> ";" <declaration_block> 

<block>::= DECLARATIONS <declaration_block> | CODE <statement_list>

<program>::= <identifier> ":" <block> ENDP <identifier> "."
